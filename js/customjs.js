$(document).ready(function () {
  $("#content").load("content/change_pin.php");

  $("ul#changecontent li a").click(function () {
    var page = $(this).attr("href");
    $("#content").load("content/" + page + ".php");
    return false;
  });
  $(".profile-usermenu li a").click(function (e) {
    $(".profile-usermenu li.active").removeClass("active");
    var $parent = $(this).parent();
    $parent.addClass("active");
    e.preventDefault();
  });

  animate(send).then(() => animate(recv));
  $(window).scroll(animate(node))
});

function animate(node) {
  return new Promise(function(resolve) {
      var i = 0;
      (function loop() {
          node.value = i;
          if (i < 100) {
              i++;
              requestAnimationFrame(loop);
          } else {
              resolve();
          }
      })();
  });
}

// PASSWORD SHOW HIDE

function old_password_show_hide() {
  var x = document.getElementById("old-password");
  var show_eye = document.getElementById("old_show_eye");
  var hide_eye = document.getElementById("old_hide_eye");
  hide_eye.classList.remove("d-none");
  if (x.type === "password") {
    x.type = "text";
    show_eye.style.display = "none";
    hide_eye.style.display = "block";
  } else {
    x.type = "password";
    show_eye.style.display = "block";
    hide_eye.style.display = "none";
  }
}

function new_password_show_hide() {
  var x = document.getElementById("new-password");
  var show_eye = document.getElementById("new_show_eye");
  var hide_eye = document.getElementById("new_hide_eye");
  hide_eye.classList.remove("d-none");
  if (x.type === "password") {
    x.type = "text";
    show_eye.style.display = "none";
    hide_eye.style.display = "block";
  } else {
    x.type = "password";
    show_eye.style.display = "block";
    hide_eye.style.display = "none";
  }
}

function verify_password_show_hide() {
  var x = document.getElementById("verify-password");
  var show_eye = document.getElementById("verify_show_eye");
  var hide_eye = document.getElementById("verify_hide_eye");
  hide_eye.classList.remove("d-none");
  if (x.type === "password") {
    x.type = "text";
    show_eye.style.display = "none";
    hide_eye.style.display = "block";
  } else {
    x.type = "password";
    show_eye.style.display = "block";
    hide_eye.style.display = "none";
  }
}

// PIN SHOW HIDE

function old_pin_show_hide() {
  var x = document.getElementById("old-pin");
  var show_eye = document.getElementById("old_pin_show_eye");
  var hide_eye = document.getElementById("old_pin_hide_eye");
  hide_eye.classList.remove("d-none");
  if (x.type === "password") {
    x.type = "text";
    show_eye.style.display = "none";
    hide_eye.style.display = "block";
  } else {
    x.type = "password";
    show_eye.style.display = "block";
    hide_eye.style.display = "none";
  }
}

function new_pin_show_hide() {
  var x = document.getElementById("new-pin");
  var show_eye = document.getElementById("new_pin_show_eye");
  var hide_eye = document.getElementById("new_pin_hide_eye");
  hide_eye.classList.remove("d-none");
  if (x.type === "password") {
    x.type = "text";
    show_eye.style.display = "none";
    hide_eye.style.display = "block";
  } else {
    x.type = "password";
    show_eye.style.display = "block";
    hide_eye.style.display = "none";
  }
}

function verify_pin_show_hide() {
  var x = document.getElementById("verify-pin");
  var show_eye = document.getElementById("verify_pin_show_eye");
  var hide_eye = document.getElementById("verify_pin_hide_eye");
  hide_eye.classList.remove("d-none");
  if (x.type === "password") {
    x.type = "text";
    show_eye.style.display = "none";
    hide_eye.style.display = "block";
  } else {
    x.type = "password";
    show_eye.style.display = "block";
    hide_eye.style.display = "none";
  }
}