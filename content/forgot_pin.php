<div id="forgot_pin" class="profile-content">
  <h4 class="title">FORGOT PIN</h4>
  <p class="subtitle">PLEASE CHOOSE : </p>
  <form action="" method="post" id="passwordForm" autocomplete="off">
    <!-- OPTION 2 -->
    <div class="row mb-3">
      <div class="col-6">
        <h4>ID MITRA</h4>
        <div class="input-group mb-3">
          <input name="text" type="text" value="LX5552" class="input form-control" id="id_mitra" disabled />
        </div>
      </div>
    </div>
    <div class="row mb-4">
      <div class="col-6">
        <h4>EMAIL MITRA</h4>
        <div class="input-group">
          <input name="email" type="email" value="email@email.com" class="input form-control" id="email_mitra" disabled />
        </div>
      </div>
      <div class="col-6">
        <h4>PHONE NUMBER</h4>
        <div class="input-group">
          <input name="text" type="text" value="081234567890" class="input form-control" id="phone_number_mitra" disabled />
        </div>
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-6">
        <h4>SEND PIN TO:</h4>
        <div class="input-group mb-3">
          <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
            <option selected>Choose...</option>
            <option value="1">EMAIL</option>
            <option value="2">PHONE NUMBER</option>
          </select>
        </div>
      </div>
    </div>
    <button type="submit" class="btn btn-dark d-flex mx-auto">Send Code</button>
  </form>
</div>