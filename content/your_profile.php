<div id="profileContent" class="profile-content">
  <h4 class="title">PROFILE</h4>
  <div class="table-responsive">
    <table class="table table-sm table-content">
      <tbody>
        <tr>
          <td>AGENT STATUS</td>
          <td>:</td>
          <td>LX52222 (BUSSINESS)</td>
        </tr>
        <tr>
          <td>ID MITRA</td>
          <td>:</td>
          <td>PRG2222 - STOKIS</td>
        </tr>
        <tr>
          <td>CITY</td>
          <td>:</td>
          <td>USA</td>
        </tr>
        <tr>
          <td>SPONSOR ID</td>
          <td>:</td>
          <td>LXXXXXX - INA</td>
        </tr>
        <tr>
          <td>UPLINE ID</td>
          <td>:</td>
          <td>BWOOO - ANI</td>
        </tr>
        <tr>
          <td>AGENCY PHONE NUMBER</td>
          <td>:</td>
          <td>+62-0812-3456-7890</td>
        </tr>
        <tr>
          <td>BONUS PHONE NUMBER</td>
          <td>:</td>
          <td>+62-0812-3456-7890</td>
        </tr>
        <tr>
          <td>EMAIL</td>
          <td>:</td>
          <td>email@email.com</td>
        </tr>
        <tr>
          <td>BANK ACCOUNT</td>
          <td>:</td>
          <td>MANDIRI - 1234567890 AN. RAHMET ABABIL SAIDAULLOH PUTRA</td>
        </tr>
        <tr>
          <td>RO STATUS</td>
          <td>:</td>
          <td>LX52222 (BUSSINESS)</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>