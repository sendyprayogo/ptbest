<div id="changePassword" class="container body-content">
  <div class="row justify-content-center my-4 mx-auto">
    <div class="col-12">
      <h4 class="title ">CHANGE PASSWORD</h4>
    </div>
    <div class="col-12">
      <p class="subtitle">CHOOSE A STRONG PASSWORD AND DON'T REUSE IT FOR ANOTHER ACCOUNT. <a href="#">LEARN MORE.</a></p>
    </div>
    <div class="col-12">
      <form action="" method="post" id="passwordForm" autocomplete="off">
        <div class="row mb-3">
          <div class="col">
            <h4>Old Password</h4>
            <div class="input-group mb-3">
              <input name="password" type="password" value="" class="input form-control" id="old-password" placeholder="your old password" required="true" aria-label="password" aria-describedby="basic-addon1" />
              <div class="input-group-append">
                <span class="input-group-text" onclick="old_password_show_hide();">
                  <i class="fas fa-eye" id="old_show_eye"></i>
                  <i class="fas fa-eye-slash d-none" id="old_hide_eye"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-5">
          <div class="col">
            <h4>New Password</h4>
            <div class="input-group">
              <input name="password" type="password" value="" class="input form-control" id="new-password" placeholder="your new password" required="true" aria-label="password" aria-describedby="basic-addon1" />
              <div class="input-group-append">
                <span class="input-group-text" onclick="new_password_show_hide();">
                  <i class="fas fa-eye" id="new_show_eye"></i>
                  <i class="fas fa-eye-slash d-none" id="new_hide_eye"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="col">
            <h4>Verify New Password</h4>
            <div class="input-group">
              <input name="password" type="password" value="" class="input form-control" id="verify-password" placeholder="verify your new password" required="true" aria-label="password" aria-describedby="basic-addon1" />
              <div class="input-group-append">
                <span class="input-group-text" onclick="verify_password_show_hide();">
                  <i class="fas fa-eye" id="verify_show_eye"></i>
                  <i class="fas fa-eye-slash d-none" id="verify_hide_eye"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-dark d-flex mx-auto">Change Password</button>
      </form>
    </div>
  </div>
</div>